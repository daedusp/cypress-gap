const customHeaders= {'Authorization': 'Basic c2hvcG1hbmFnZXI6YXhZMiByaW1jIFN6TzkgY29iZiBBWkJ3IE5Mblg=',
                        'Content-Type': 'application/json'}

before(() => {
    var postData =  "{\r\n  \"name\": \"DanielSolisPrado\",\r\n  \"type\": \"simple\",\r\n  \"regular_price\": \"21.99\",\r\n  \"description\": \"simple\",\r\n  \"short_description\": \"ss\",\r\n  \"categories\": [\r\n    {\r\n      \"id\": 9\r\n    },\r\n    {\r\n      \"id\": 14\r\n    }\r\n  ],\r\n  \"images\": [\r\n    {\r\n      \"src\": \"http://demo.woothemes.com/woocommerce/wp-content/uploads/sites/56/2013/06/T_2_front.jpg\"\r\n    },\r\n    {\r\n      \"src\": \"http://demo.woothemes.com/woocommerce/wp-content/uploads/sites/56/2013/06/T_2_back.jpg\"\r\n    }\r\n  ]\r\n}"; 
    cy.request({method: 'POST',
                url: 'wp-json/wc/v3/products',
                body: postData,
                headers: customHeaders
                }).then((response)=>{
                    expect(response.status).equal(201)
                    cy.writeFile("cypress/fixtures/post_response.json", response.body)
                   // expect(response.body.name).equal(user.name)
                   // expect(response.body.job).equal(user.job)   
                })
  })

after(() => {
    cy.fixture('post_response.json').then(function(data) {this.data=data
    cy.request({method: 'DELETE',
                url: `wp-json/wc/v3/products/${this.data.id}`,
                headers: customHeaders
                }).then(function(response) {
                    expect(response.status).to.eq(200)
                    expect(response.body).to.not.be.null  
      }); 
   })
})

describe('Basic product workflow', () => {
    it('Adding product to Cart', () => {
        cy.fixture('post_response.json').then(function(data) {
            this.data=data
            cy.visit(`product/${this.data.slug}`)
            .title().should('eq', `${this.data.name} – QA Playground`)


            cy.get('.entry-summary .woocommerce-Price-amount')
            .should(($p) => {
              expect($p).to.contain(`${this.data.price}`)
            })

        }
        )
        cy.get('[step]')
           .clear()
           .type(7)
           .should('have.value', '7')
         
        cy.get('.single_add_to_cart_button')
           .click()
        
        cy.get('#site-header-cart .count')
        .should(($p) => {
            expect($p).to.contain('7 items')
          }).click()
        
        cy.url().should('eq', 'http://34.205.174.166/cart/');

        cy.fixture('post_response.json').then(function(data) {
            this.data=data
            cy.get('.product-name > a')
            .should(($p) => {
              expect($p).to.contain(`${this.data.name}`)
            })

            cy.get('.product-price > .woocommerce-Price-amount')
            .should(($p) => {
                expect($p).to.contain(`${this.data.price}`)
              })

              cy.get('[step]')
              .clear()
              .type(7)
              .should('have.value', '7')

              var total = this.data.price * 7

              cy.get('.product-subtotal > .woocommerce-Price-amount')
              .should(($p) => {
                expect($p).to.contain(`${total.toFixed(2)}`)
              })
            
        })
     });
 })

 
 